package com.assignment.kavya.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment.kavya.R;
import com.assignment.kavya.api.model.Activity;

public class ActivityViewHolder extends RecyclerView.ViewHolder {
    public ImageView activityImageView;
    private TextView titleTextView;

    public ActivityViewHolder(@NonNull View itemView) {
        super(itemView);
        activityImageView = itemView.findViewById(R.id.activityImageView);
        titleTextView = itemView.findViewById(R.id.titleTextView);
    }

    public void bindData(Activity activity) {
        titleTextView.setText(activity.title);
    }
}