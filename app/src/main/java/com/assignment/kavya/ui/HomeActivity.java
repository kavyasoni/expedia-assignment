package com.assignment.kavya.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.assignment.kavya.R;
import com.assignment.kavya.api.model.ActivitiesResponse;
import com.assignment.kavya.api.model.Activity;
import com.assignment.kavya.data.ActivitiesViewModel;
import com.assignment.kavya.databinding.ActivityMainBinding;
import com.assignment.kavya.ui.adapter.ActivitiesAdapter;

public class HomeActivity extends AppCompatActivity implements ActivitiesAdapter.ActivitySelectListener {
    private ActivitiesViewModel activitiesViewModel;
    private ActivitiesAdapter activitiesAdapter;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activitiesViewModel = ViewModelProviders.of(this)
                .get(ActivitiesViewModel.class);
        setupObservers();
        setUpActivitiesRecyclerView();
        binding.errorLayout.retryButton.setOnClickListener(v -> activitiesViewModel.loadActivities());
    }

    private void setUpActivitiesRecyclerView() {
        activitiesAdapter = new ActivitiesAdapter(this, this);
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation);
        binding.activitiesListLayout.activitiesRecyclerView.setLayoutAnimation(animation);
        binding.activitiesListLayout.activitiesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.activitiesListLayout.activitiesRecyclerView.setAdapter(activitiesAdapter);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        activitiesViewModel.loadActivities();
    }

    private void setupObservers() {
        activitiesViewModel.getShowErrorLiveData().observe(this, this::showErrorView);
        activitiesViewModel.getShowLoadingLiveData().observe(this, this::showLoadingView);
        activitiesViewModel.getActivitiesResponseLiveData().observe(this, this::showActivitiesView);
    }

    private void showActivitiesView(ActivitiesResponse activitiesResponse) {
        binding.activitiesListLayout.baseLayout.setVisibility(View.VISIBLE);
        activitiesAdapter.setActivities(activitiesResponse.activities);
    }

    private void showErrorView(Boolean show) {
        binding.errorLayout.baseLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void showLoadingView(Boolean show) {
        if (show) {
            binding.errorLayout.baseLayout.setVisibility(View.GONE);
            binding.activitiesListLayout.baseLayout.setVisibility(View.GONE);
        }
        binding.loadingLayout.baseLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onActivitySelected(Activity activity) {
        Intent activityDetailIntent = new Intent(this, DetailActivity.class);
        activityDetailIntent.putExtra("ACTIVITY_ID", activity.id);
        startActivity(activityDetailIntent);
    }
}
