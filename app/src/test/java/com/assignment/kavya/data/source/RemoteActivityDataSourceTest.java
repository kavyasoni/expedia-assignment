package com.assignment.kavya.data.source;

import com.assignment.kavya.BaseObservableTest;
import com.assignment.kavya.api.ActivitiesService;
import com.assignment.kavya.api.model.ActivitiesResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static com.assignment.kavya.Util.getMockActivitiesResponse;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class RemoteActivityDataSourceTest extends BaseObservableTest {
    @Mock
    private ActivitiesService activitiesService;

    private RemoteActivitiesDataSource remoteActivitiesDataSource;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        remoteActivitiesDataSource = new RemoteActivitiesDataSource(activitiesService);
    }

    @After
    public void tearDown() {
        remoteActivitiesDataSource = null;
    }

    @Test
    public void test_GetActivities_with_errorInService() {
        Exception exception = new Exception("Random API error");
        when(remoteActivitiesDataSource.getActivities(anyString())).thenReturn(Single.error(exception));

        TestObserver<ActivitiesResponse> testObserver = TestObserver.create();
        remoteActivitiesDataSource.getActivities(anyString()).subscribe(testObserver);
        testObserver.assertError(exception);
    }

    @Test
    public void test_GetActivities_with_validResponse() {
        ActivitiesResponse activitiesResponse = getMockActivitiesResponse();

        when(remoteActivitiesDataSource.getActivities(anyString())).thenReturn(Single.just(activitiesResponse));

        TestObserver<ActivitiesResponse> testObserver = TestObserver.create();
        remoteActivitiesDataSource.getActivities(anyString()).subscribe(testObserver);
        testObserver.assertNoErrors();
        testObserver.assertValue(activitiesResponse);
    }
}