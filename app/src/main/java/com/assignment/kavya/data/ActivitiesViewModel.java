package com.assignment.kavya.data;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.assignment.kavya.ExpediaApplication;
import com.assignment.kavya.api.model.ActivitiesResponse;
import com.assignment.kavya.api.model.ActivityDetail;
import com.assignment.kavya.data.source.RemoteActivitiesDataSource;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ActivitiesViewModel extends AndroidViewModel {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final MutableLiveData<ActivitiesResponse> activitiesResponseLiveData;
    private final MutableLiveData<ActivityDetail> activityDetailLiveData;
    private final MutableLiveData<Boolean> showLoadingLiveData;
    private final MutableLiveData<Boolean> showErrorLiveData;
    private final RemoteActivitiesDataSource activityDataSource;

    public ActivitiesViewModel(Application application) {
        super(application);
        ExpediaApplication expediaApplication = (ExpediaApplication) application;
        this.activityDataSource = expediaApplication.getActivityDataSource();
        this.activitiesResponseLiveData = new MutableLiveData<>();
        this.activityDetailLiveData = new MutableLiveData<>();
        this.showLoadingLiveData = new MutableLiveData<>();
        this.showErrorLiveData = new MutableLiveData<>();
    }

    public LiveData<ActivitiesResponse> getActivitiesResponseLiveData() {
        return activitiesResponseLiveData;
    }

    public LiveData<ActivityDetail> getActivityDetailLiveData() {
        return activityDetailLiveData;
    }

    public LiveData<Boolean> getShowLoadingLiveData() {
        return showLoadingLiveData;
    }

    public MutableLiveData<Boolean> getShowErrorLiveData() {
        return showErrorLiveData;
    }

    public void loadActivities() {
        loadActivities("Seattle");
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }

    /**
     * Method to load activities in given location.
     *
     * @param location
     */
    public void loadActivities(String location) {
        showLoadingLiveData.setValue(true);
        showErrorLiveData.setValue(false);
        compositeDisposable.add(activityDataSource.getActivities(location)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(activitiesResponse -> {
                            activitiesResponseLiveData.setValue(activitiesResponse);
                            showLoadingLiveData.setValue(false);
                        }, throwable -> {
                            showLoadingLiveData.setValue(false);
                            showErrorLiveData.setValue(true);
                        }
                ));
    }

    /**
     * Method to load activity detail in with given activityId.
     *
     * @param activityId
     */
    public void loadActivityDetail(String activityId) {
        showLoadingLiveData.setValue(true);
        showErrorLiveData.setValue(false);
        compositeDisposable.add(activityDataSource.getActivityDetail(activityId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(activityDetail -> {
                            activityDetailLiveData.setValue(activityDetail);
                            showLoadingLiveData.setValue(false);
                        }, throwable -> {
                            showLoadingLiveData.setValue(false);
                            showErrorLiveData.setValue(true);
                        }
                ));
    }
}
