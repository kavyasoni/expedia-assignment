package com.assignment.kavya.api.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Image implements Parcelable {
    public String url;
    public String caption;
    public boolean isImage;

    protected Image(Parcel in) {
        url = in.readString();
        caption = in.readString();
        isImage = in.readByte() != 0;
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(caption);
        dest.writeByte((byte) (isImage ? 1 : 0));
    }
}
