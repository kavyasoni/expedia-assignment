package com.assignment.kavya;

import com.assignment.kavya.api.model.ActivitiesResponse;
import com.assignment.kavya.api.model.Activity;

import org.jetbrains.annotations.NotNull;
import org.mockito.Mockito;

import java.util.ArrayList;

public class Util {
    @NotNull
    public static ActivitiesResponse getMockActivitiesResponse() {
        ActivitiesResponse activitiesResponse = Mockito.mock(ActivitiesResponse.class);
        Activity activity = Mockito.mock(Activity.class);
        activitiesResponse.activities = new ArrayList<>();
        activitiesResponse.activities.add(activity);
        return activitiesResponse;
    }
}
