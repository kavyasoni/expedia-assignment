package com.assignment.kavya;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import org.junit.Rule;

public class BaseObservableTest {
    @Rule
    public RxJavaImmediateReturnRule rxJavaImmediateReturnRule = new RxJavaImmediateReturnRule();

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();
}