package com.assignment.kavya.data;

import android.arch.lifecycle.Observer;

import com.assignment.kavya.BaseObservableTest;
import com.assignment.kavya.ExpediaApplication;
import com.assignment.kavya.api.model.ActivitiesResponse;
import com.assignment.kavya.data.source.RemoteActivitiesDataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Single;

import static com.assignment.kavya.Util.getMockActivitiesResponse;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class ActivitiesViewModelTest extends BaseObservableTest {
    private ActivitiesViewModel activitiesViewModel;

    @Mock
    private ExpediaApplication application;

    @Mock
    private RemoteActivitiesDataSource remoteActivitiesDataSource;

    @Mock
    private Observer<ActivitiesResponse> activitiesResponseObserver;

    @Mock
    private Observer<Boolean> loadBooleanObserver;

    @Mock
    private Observer<Boolean> errorBooleanObserver;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(application.getActivityDataSource()).thenReturn(remoteActivitiesDataSource);
        activitiesViewModel = new ActivitiesViewModel(application);
        activitiesViewModel.getShowErrorLiveData().observeForever(errorBooleanObserver);
        activitiesViewModel.getShowLoadingLiveData().observeForever(loadBooleanObserver);
        activitiesViewModel.getActivitiesResponseLiveData().observeForever(activitiesResponseObserver);
    }

    @After
    public void tearDown() {
        activitiesViewModel.getShowErrorLiveData().removeObserver(errorBooleanObserver);
        activitiesViewModel.getShowLoadingLiveData().removeObserver(loadBooleanObserver);
        activitiesViewModel.getActivitiesResponseLiveData().removeObserver(activitiesResponseObserver);
        activitiesViewModel.onCleared();
        activitiesViewModel = null;
    }

    @Test
    public void test_GetActivities_with_errorFromAPI() {
        when(remoteActivitiesDataSource.getActivities(anyString())).thenReturn(Single.error(new Exception("Random API error")));

        activitiesViewModel.loadActivities();

        verify(errorBooleanObserver).onChanged(false);
        verify(loadBooleanObserver).onChanged(true);

        verify(errorBooleanObserver).onChanged(true);
        verify(loadBooleanObserver).onChanged(false);
    }

    @Test
    public void test_GetActivities_with_validResponseFromAPI() {
        ActivitiesResponse activitiesResponse = getMockActivitiesResponse();

        when(remoteActivitiesDataSource.getActivities(anyString())).thenReturn(Single.just(activitiesResponse));

        activitiesViewModel.loadActivities();

        verify(errorBooleanObserver).onChanged(false);
        verify(loadBooleanObserver).onChanged(true);

        verify(errorBooleanObserver).onChanged(false);
        verify(loadBooleanObserver).onChanged(false);

        verify(activitiesResponseObserver).onChanged(activitiesResponse);
    }
}