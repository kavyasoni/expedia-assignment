package com.assignment.kavya.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.assignment.kavya.R;
import com.assignment.kavya.api.model.ActivityDetail;
import com.assignment.kavya.data.ActivitiesViewModel;
import com.assignment.kavya.databinding.ActivityDetailBinding;
import com.assignment.kavya.ui.adapter.OfferAdapter;
import com.bumptech.glide.Glide;

public class DetailActivity extends AppCompatActivity {
    private ActivitiesViewModel activitiesViewModel;
    private ActivityDetailBinding binding;
    private OfferAdapter offerAdapter;
    private String activityId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        activitiesViewModel = ViewModelProviders.of(this)
                .get(ActivitiesViewModel.class);
        setupObservers();
        setUpOffersRecyclerView();
        activityId = getIntent().getExtras().getString("ACTIVITY_ID");
        binding.errorLayout.retryButton.setOnClickListener(v -> activitiesViewModel.loadActivityDetail(activityId));
    }

    private void setUpOffersRecyclerView() {
        offerAdapter = new OfferAdapter(this);
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation);
        binding.activitiesDetailLayout.offersRecyclerView.setLayoutAnimation(animation);
        binding.activitiesDetailLayout.offersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.activitiesDetailLayout.offersRecyclerView.setAdapter(offerAdapter);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        activitiesViewModel.loadActivityDetail(activityId);
    }

    private void setupObservers() {
        activitiesViewModel.getShowErrorLiveData().observe(this, this::showErrorView);
        activitiesViewModel.getShowLoadingLiveData().observe(this, this::showLoadingView);
        activitiesViewModel.getActivityDetailLiveData().observe(this, this::showActivityDetails);
    }

    private void showActivityDetails(ActivityDetail activityDetail) {
        binding.activitiesDetailLayout.baseLayout.setVisibility(View.VISIBLE);
        binding.activitiesDetailLayout.activityTitle.setText(activityDetail.title);
        binding.activitiesDetailLayout.activityDetail.setText(Html.fromHtml(activityDetail.description));
        Glide.with(this)
                .load(activityDetail.getActivityImage())
                .into(binding.activitiesDetailLayout.activityImageView);
        offerAdapter.setOffers(activityDetail.offers);
    }

    private void showErrorView(Boolean show) {
        binding.errorLayout.baseLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void showLoadingView(Boolean show) {
        if (show) {
            binding.errorLayout.baseLayout.setVisibility(View.GONE);
            binding.activitiesDetailLayout.baseLayout.setVisibility(View.GONE);
        }
        binding.loadingLayout.baseLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
