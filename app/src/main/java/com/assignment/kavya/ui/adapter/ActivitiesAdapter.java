package com.assignment.kavya.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.assignment.kavya.R;
import com.assignment.kavya.api.model.Activity;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class ActivitiesAdapter extends RecyclerView.Adapter<ActivityViewHolder> {
    private final ActivitySelectListener activitySelectListener;
    private List<Activity> activities = new ArrayList<>();
    private final LayoutInflater layoutInflater;
    private final Context context;

    public ActivitiesAdapter(Context context, ActivitySelectListener activitySelectListener) {
        this.activitySelectListener = activitySelectListener;
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public ActivityViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ActivityViewHolder(layoutInflater.inflate(R.layout.activity_item_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityViewHolder activityViewHolder, int i) {
        Activity activity = activities.get(i);
        activityViewHolder.bindData(activity);
        Glide.with(context)
                .load(activity.imageUrl)
                .into(activityViewHolder.activityImageView);
        activityViewHolder.itemView.setOnClickListener(v -> activitySelectListener.onActivitySelected(activity));
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    public void setActivities(List<Activity> activities) {
        this.activities.clear();
        this.activities.addAll(activities);
        this.notifyDataSetChanged();
    }

    public interface ActivitySelectListener {
        void onActivitySelected(Activity activity);
    }
}
