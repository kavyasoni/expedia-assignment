package com.assignment.kavya.data.source;

import com.assignment.kavya.api.ActivitiesService;
import com.assignment.kavya.api.model.ActivitiesResponse;
import com.assignment.kavya.api.model.ActivityDetail;

import io.reactivex.Single;

public class RemoteActivitiesDataSource {
    private final ActivitiesService activitiesService;

    public RemoteActivitiesDataSource(ActivitiesService activitiesService) {
        this.activitiesService = activitiesService;
    }

    public Single<ActivitiesResponse> getActivities(String location) {
        return activitiesService.getActivities(location, "2019-03-18", "2019-03-30");
    }

    public Single<ActivityDetail> getActivityDetail(String activityId) {
        return activitiesService.getActivityDetail(activityId, "2019-03-18", "2019-03-30");
    }
}
