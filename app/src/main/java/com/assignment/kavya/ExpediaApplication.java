package com.assignment.kavya;

import android.app.Application;

import com.assignment.kavya.api.ActivitiesServiceImp;
import com.assignment.kavya.data.source.RemoteActivitiesDataSource;

public class ExpediaApplication extends Application {
    private RemoteActivitiesDataSource remoteActivitiesDataSource;


    public RemoteActivitiesDataSource getActivityDataSource() {
        if (remoteActivitiesDataSource == null) {
            remoteActivitiesDataSource = new RemoteActivitiesDataSource(ActivitiesServiceImp.getInstance());
        }
        return remoteActivitiesDataSource;
    }
}
