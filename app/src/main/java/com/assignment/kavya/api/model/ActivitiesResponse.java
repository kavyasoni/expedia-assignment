package com.assignment.kavya.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class ActivitiesResponse implements Parcelable {
    public List<Activity> activities;

    protected ActivitiesResponse(Parcel in) {
        activities = in.createTypedArrayList(Activity.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(activities);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ActivitiesResponse> CREATOR = new Creator<ActivitiesResponse>() {
        @Override
        public ActivitiesResponse createFromParcel(Parcel in) {
            return new ActivitiesResponse(in);
        }

        @Override
        public ActivitiesResponse[] newArray(int size) {
            return new ActivitiesResponse[size];
        }
    };
}
