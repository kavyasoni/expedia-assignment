package com.assignment.kavya.api;

import com.assignment.kavya.BuildConfig;
import com.assignment.kavya.api.model.ActivitiesResponse;
import com.assignment.kavya.api.model.ActivityDetail;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ActivitiesServiceImp implements ActivitiesService {
    private static ActivitiesServiceImp activityService;
    private Retrofit retrofit;

    private ActivitiesServiceImp() {

        // Gson for ConverterFactory
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        // Interceptor to log request and response
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        HttpLoggingInterceptor.Level level = BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE;
        httpLoggingInterceptor.setLevel(level);

        // OkHttpClient to add Network Interceptor
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addNetworkInterceptor(httpLoggingInterceptor);

        retrofit = new Retrofit.Builder()
                .client(builder.build())
                .baseUrl("https://www.expedia.com/lx/api/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static ActivitiesService getInstance() {
        if (activityService == null) {
            activityService = new ActivitiesServiceImp();
        }
        return activityService;
    }


    @Override
    public Single<ActivitiesResponse> getActivities(String location, String startDate, String endDate) {
        return retrofit.create(ActivityAPI.class).getActivities(location, startDate, endDate);
    }

    @Override
    public Single<ActivityDetail> getActivityDetail(String activityId, String startDate, String endDate) {
        return retrofit.create(ActivityAPI.class).getActivityDetail(activityId, startDate, endDate);
    }

    public interface ActivityAPI {

        @GET("search")
        Single<ActivitiesResponse> getActivities(@Query("location") String location, @Query("startDate") String startDate, @Query("endDate") String endDate);

        @GET("activity")
        Single<ActivityDetail> getActivityDetail(@Query("activityId") String activityId, @Query("startDate") String startDate, @Query("endDate") String endDate);
    }
}
