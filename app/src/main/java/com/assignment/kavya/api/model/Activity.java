package com.assignment.kavya.api.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Activity implements Parcelable {
    public String id;
    public String title;
    public String duration;
    public String imageUrl;
    public String fromPrice;
    public String largeImageURL;
    public String discountPercentage;

    protected Activity(Parcel in) {
        id = in.readString();
        title = in.readString();
        duration = in.readString();
        imageUrl = in.readString();
        fromPrice = in.readString();
        largeImageURL = in.readString();
        discountPercentage = in.readString();
    }

    public static final Creator<Activity> CREATOR = new Creator<Activity>() {
        @Override
        public Activity createFromParcel(Parcel in) {
            return new Activity(in);
        }

        @Override
        public Activity[] newArray(int size) {
            return new Activity[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(duration);
        dest.writeString(imageUrl);
        dest.writeString(fromPrice);
        dest.writeString(largeImageURL);
        dest.writeString(discountPercentage);
    }
}
