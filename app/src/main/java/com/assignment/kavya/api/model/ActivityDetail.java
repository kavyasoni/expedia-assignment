package com.assignment.kavya.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import java.util.Random;

public class ActivityDetail extends Activity implements Parcelable {
    public String description;
    public List<Offer> offers;
    public List<Image> images;

    protected ActivityDetail(Parcel in) {
        super(in);
        offers = in.createTypedArrayList(Offer.CREATOR);
        images = in.createTypedArrayList(Image.CREATOR);
        description = in.readString();
    }

    public static final Creator<Activity> CREATOR = new Creator<Activity>() {
        @Override
        public Activity createFromParcel(Parcel in) {
            return new Activity(in);
        }

        @Override
        public Activity[] newArray(int size) {
            return new Activity[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(offers);
        dest.writeTypedList(images);
        dest.writeString(description);
    }

    public String getActivityImage() {
        Random random = new Random();
        if (images == null || images.isEmpty())
            return null;
        return images.get(random.nextInt(images.size())).url;
    }
}
