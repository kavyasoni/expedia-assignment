package com.assignment.kavya.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment.kavya.R;
import com.assignment.kavya.api.model.Offer;

import java.util.ArrayList;
import java.util.List;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.OfferViewHolder> {
    private List<Offer> offers = new ArrayList<>();
    private final LayoutInflater layoutInflater;

    public OfferAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public OfferViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new OfferViewHolder(layoutInflater.inflate(R.layout.offer_item_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OfferViewHolder offerViewHolder, int i) {
        offerViewHolder.bindData(offers.get(i));
    }

    @Override
    public int getItemCount() {
        return offers.size();
    }

    public void setOffers(List<Offer> offers) {
        if (offers == null || offers.isEmpty())
            return;
        this.offers.clear();
        this.offers.addAll(offers);
        this.notifyDataSetChanged();
    }

    public class OfferViewHolder extends RecyclerView.ViewHolder {
        private TextView titleTextView;

        public OfferViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
        }

        public void bindData(Offer offer) {
            titleTextView.setText(offer.title);
        }
    }
}
