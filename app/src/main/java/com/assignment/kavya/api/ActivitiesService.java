package com.assignment.kavya.api;

import com.assignment.kavya.api.model.ActivitiesResponse;
import com.assignment.kavya.api.model.ActivityDetail;

import io.reactivex.Single;

public interface ActivitiesService {
    Single<ActivitiesResponse> getActivities(String location, String startDate, String endDate);

    Single<ActivityDetail> getActivityDetail(String activityId, String startDate, String endDate);
}
